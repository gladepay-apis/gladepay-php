# About GladePay

GladePay is the leading digital payments provider with focus across African countries, dedicated to creating simple payments solution for African businesses. We are focused on building trust between buyers and sellers by providing simple solutions using the payment options (online or offline) preferred with complete security and efficiency.

To Learn more, visit [https://developer.glade.ng/](https://developer.glade.ng/).

## Getting Started
You need a Merchant ID and Key to authenticate against the API, please contact support@gladepay.com to setup a demo account.

## Installation

You can close this project and include the class in your project or you can add the repo in your `composer.json` file as in the example below:

```
{
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/gladepay-apis/gladepay-php"
        }
    ],
    "require": {
        "gladepay/gladepay": "^1.0.0"
    }
}
```

Or simply run the command `composer require gladepay/gladepay` in your project.

## Example



Initiate Class:
```
$gp = new GladePay\GladePay("MERCHANT-ID", "MERCHANT-KEY", "https://demo.api.gladepay.com");

```

### For Card Payments:

```

$json_initiate = '
{
    "action":"initiate",
    "paymentType":"card",
    "user": {
        "firstname":"Abubakar",
        "lastname":"Ango",
        "email":"test@gladepay.com",
        "ip":"192.168.33.10",
        "fingerprint": "cccvxbxbxb"
    },
    "card":{
        "card_no":"5438898014560229",
        "expiry_month":"09",
        "expiry_year":"19",
        "ccv":"789",
        "pin":"3310"
    },
    "amount":"10000",
    "country": "NG",
    "currency": "NGN"
}
';


$json_request = json_decode($json_initiate, true);

//Initiate a Card Payment Transaction
$gp->cardPayment($json_request['user'], $json_request['card'], $json_request['amount'], $json_request['country'], $json_request['currency']);

//If an OTP is required, You can pass the OTP along witha a transaction reference returned by the last transaction
$gp->validateOTP("GP270019221Y","12345");

//After all is done, verify to confirm final status of the transaction.
$gp->verifyTransaction("GP270019221Y");

```

If you will be charging with a previosly saved token:

```
$gp->chargeWithToken('TOKEN', $json_request['user'], $json_request['amount'] )
```

### For Accounts Payment:

```

$json_account = '
{
    "action":"initiate",
    "paymentType":"card",
    "user": {
        "firstname":"Abubakar",
        "lastname":"Ango",
        "email":"test@gladepay.com",
        "ip":"192.168.33.10",
        "fingerprint": "cccvxbxbxb"
    },
    "account":{
      "accountnumber":"0690000007",
      "bankcode":"044"
  },
    "amount":"10000",
    "country": "NG",
    "currency": "NGN"
}
';


$json_request = json_decode($json_account, true);

$gp->accountPayment($json_request['user'], '$json_request['account'], $json_request['amount']);

//After successful completion, you can verify the status of the payment
$gp->validateAccountPayment($txnRef);

```

### Other methods

Get list of all Banks:
```
$gp->getAllBanks();
```

Get list of banks that support account payments:
```
$gp->getAccountPaymentSupportedBanks();
```

Get Details of a card:
```
$gp->getCardDetails($card_number);
```

Get the charges applicable to a card:
```
$gp->getCharges($paymentType, $amount, $card_no);
```

Perform Money transfer:
```
$gp->moneyTransfer($amount, $bankcode, $accountnumber, $sender_name, $narration);
```

Verify status of Money transfer:
```
$gp->verifyMoneyTransfer($txnRef);
```

## Return Values
All methods return an array.
