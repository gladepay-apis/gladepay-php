<?php

namespace GladePay;

class GladePay{

    private $client;

    public function __construct( $merchant_id, $merchant_key, $base_url){
        
        $headers = ['mid' => $merchant_id, 'key' => $merchant_key];
        $this->client = new \GuzzleHttp\Client(['Content-Type' => 'application/json', 'base_uri' => $base_url, 'headers' => $headers]);
    }

    public function cardPayment($user_details, $card_details, $amount, $country, $currency, $type = false, $type_data = false){
        //$type can be false, recurrent, installment

        $request = [ 'user' => $user_details, 'card' => $card_details, 'amount' => $amount, 'country' => $country, 'currency' => $currency];

        //print_r($user_details); exit();

        $initalResponse = json_decode($this->initiateTransaction($request, $type, $type_data), true);

        if(key_exists('status', $initalResponse)){
            if($initalResponse['status'] == 202){
                $chargeResponse =  json_decode($this->chargeCard($request, $initalResponse['txnRef'], $initalResponse['apply_auth']), true);

                if(key_exists('status', $chargeResponse)){
                    if($chargeResponse['status'] == 202){
                        if(key_exists('validate', $chargeResponse)){
                            return json_encode([
                                "status" => 202,
                                "txnRef" => $chargeResponse['txnRef'],
                                "message" => "Please require the user to enter an OTP and call `validateOTP` with the `txnRef`"
                            ]);
                        }else if (key_exists('authURL', $chargeResponse)){
                            return json_encode([
                                "status" => 202,
                                "txnRef" => $chargeResponse['txnRef'],
                                "authURL" => $chargeResponse['authURL'],
                                "message" => "Please load the link contained in `authURL` for the user to validate Payment"
                            ]);
                        }else{
                            return json_encode([
                                "status" => 500,
                                "message" => "Unrecognized Response from Gateway."
                            ]);
                        }

                    }
                }
            }else{
                return json_encode([
                    "status" => 500,
                    "message" => $initalResponse['message']
                ]);
            }
        }else{
            return json_encode([
                "status" => 500,
                "message" => "Unrecognized Response from Gateway."
            ]);
        }
    
    }



    private function initiateTransaction($request, $type, $type_data)
    {
        $data = [
            "action" => "initiate",
            "paymentType" => "card",
            "user" => $request['user'],
            "card" => $request['card'],
            "amount"=> $request['amount'],
            "country"=>$request['country'],
            "currency"=>$request['currency']
        ];

        switch ($type){
            case 'recurrent':
                $request_data = array_merge($data, ['recurrent' => $type_data]);
            break;
            case 'installment':
                $request_data = array_merge($data, ['installment' => $type_data]);
            break;
            default:
                $request_data = $data;
            break;
        }

        return $this->callAPI('PUT', 'payment', $request_data);
    }

    private function chargeCard($request, $txn_ref, $auth_type)
    {
        $request_data = [
            "action" => "charge",
            "paymentType" => "card",
            "user" => $request['user'],
            "card" => $request['card'],
            "amount"=> $request['amount'],
            "country"=>$request['country'],
            "currency"=>$request['currency'],
            "txnRef"=>$txn_ref,
            "auth_type" => $auth_type
        ];

        return $this->callAPI('PUT', 'payment', $request_data);
    }

    public function chargeWithToken($token, $user_details, $amount){
        $request_data =[
            "action" => "charge",
            "paymentType" => "token",
            "token" => $token,
            "user" => $user_details,
            "amount" => $amount
        ];

        $tokenResponse = $this->callAPI('PUT', 'payment', $request_data);

        if(key_exists('status', $tokenResponse)){
            if($tokenResponse['status'] == 200){
                return json_encode([
                    "status" => 200,
                    "txnRef" => $tokenResponse['txnRef'],
                    "message" => "Successful Payment."
                ]);
            }else{
                return json_encode([
                    "status" => 500,
                    "message" => "Error processing"
                ]);
            }

        }else{
            return json_encode([
                "status" => 500,
                "message" => "Unrecognized Response from Gateway."
            ]);
        }
    }

    public function validateOTP($txnRef, $otp)
    {
        $request_data = [
            "action" => "validate",
            "txnRef" => $txnRef,
            "otp" => $otp
        ];

        return $this->callAPI('PUT', 'payment', $request_data);
    }

    public function verifyTransaction($txnRef)
    {
        $request_data = [
            "action" => "verify",
            "txnRef" => $txnRef
        ];

        return $this->callAPI('PUT', 'payment', $request_data);
    }


    public function accountPayment($user_details, $account_details, $amount){
        $request_data = [
            "action" => "charge",
            "paymentType" => "account",
            "user" => $user_details,
            "account" => $account_details,
            "amount"=> $amount
        ];

        return $this->callAPI('PUT', 'payment', $request_data);
    }

    public function validateAccountPayment($txnRef){
        $request_data = [
                "action" => "validate",
                "txnRef" => $txnRef,
                "validate" => "account",
                "otp" => "12345"
        ];
    }

    public function getAccountPaymentSupportedBanks(){
        $request_data = [
            "inquire" => "supported_chargable_banks"
        ];

        return $this->callAPI('PUT', 'resources', $request_data);
    }

    public function getAllBanks(){
        $request_data = [
            "inquire" => "banks"
        ];

        return $this->callAPI('PUT', 'resources', $request_data);
    }

    public function getCardDetails($card_number){
        $request_data = [
            "inquire"=> "card", "card_no" => $card_number
        ];

        return $this->callAPI('PUT', 'resources', $request_data);
    }

    public function getCharges($paymentType, $amount, $card_no = false){
        //Card number can not be false where $paymentType is card

        $request_data = [
            "inquire" => "charges",
            "amount" => $amount
        ];

        if($paymentType == "card"){
            $request_data += ["card_no" => $card_no];
        } else{
            $request_data += ["type" => $paymentType];
        }

        return $this->callAPI('PUT', 'resources', $request_data);
    }

    public function moneyTransfer($amount, $bankcode, $accountnumber, $sender_name, $narration){
        $request_data = [
            "action" => "transfer",
            "amount" => $amount,
            "bankcode" => $bankcode, 
            "accountnumber" =>$bankcode,
            "sender_name" => $sender_name,
            "narration" => $narration
        ];

        return $this->callAPI('PUT', 'disburse', $request_data);
    }

    public function verifyMoneyTransfer($txnRef){
        $request_data = [
            "action" => "verify",
            "txnRef" => $txnRef
        ];

        return $this->callAPI('PUT', 'disburse', $request_data);
    }

    private function callAPI($method, $api_method, $data){
        $response = $this->client->request($method, '/'.$api_method, ['body' => json_encode($data)]);

        return json_decode($response->getBody());
    }
}